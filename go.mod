module gitlab.com/welance/oss/distill

require (
	github.com/AndreasBriese/bbloom v0.0.0-20170702084017-28f7e881ca57 // indirect
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/bluele/gcache v0.0.0-20171010155617-472614239ac7
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dgraph-io/badger v1.5.1
	github.com/dgryski/go-farm v0.0.0-20180109070241-2de33835d102 // indirect
	github.com/fsnotify/fsnotify v1.4.7 // indirect
	github.com/go-chi/chi v3.3.2+incompatible
	github.com/go-chi/render v1.0.0
	github.com/golang/protobuf v1.0.0 // indirect
	github.com/hashicorp/hcl v0.0.0-20180404174102-ef8a98b0bbce // indirect
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/jbrodriguez/mlog v0.0.0-20160501155140-006dc6db226a
	github.com/magiconair/properties v1.7.6 // indirect
	github.com/matoous/go-nanoid v0.0.0-20180109130436-958d370425a1
	github.com/mitchellh/mapstructure v0.0.0-20180220230111-00c29f56e238 // indirect
	github.com/pelletier/go-toml v1.1.0 // indirect
	github.com/pkg/errors v0.8.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/spf13/afero v1.1.0 // indirect
	github.com/spf13/cast v1.2.0 // indirect
	github.com/spf13/cobra v0.0.2
	github.com/spf13/jwalterweatherman v0.0.0-20180109140146-7c0cea34c8ec // indirect
	github.com/spf13/pflag v1.0.1 // indirect
	github.com/spf13/viper v1.0.2
	github.com/stretchr/testify v1.2.2 // indirect
	golang.org/x/net v0.0.0-20180420171651-5f9ae10d9af5 // indirect
	golang.org/x/sync v0.0.0-20180314180146-1d60e4601c6f // indirect
	golang.org/x/sys v0.0.0-20180427151831-cbbc999da32d // indirect
	golang.org/x/text v0.3.0 // indirect
	gopkg.in/yaml.v2 v2.2.1
)
